# Apa itu HTML?
HyperText Markup Language (HTML) adalah bahasa untuk membuat laman web.
Biasanya laman web dilihat menggunakan browser.
Bisa berisi tulisan, tautan, gambar, bahkan lagu dan video.

HTML juga bisa digunakan untuk menambah metadata ke laman web.
Metadata adalah informasi yang tidak ditampilkan oleh browser.
Misalnya, pembuat laman web tersebut, atau bahasa yang digunakan.

HTML dibuat oleh World Wide Web Consortium (W3C).
Ada banyak versi dan variasi HTML.
Versi yang direkomendasi oleh W3C adalah HTML5.

## Editor
Dokumen HTML dapat dibuat menggunakan teks editor apa saja, bahkan Notepad.
Meskipun begitu, akan lebih baik jika menggunakan yang khusus,
dan memiliki fitur-fitur berguna seperti pewarnaan, indentasi otomatis, dll.

Berikut editor yang direkomendasi:
* [Visual Studio Code](https://code.visualstudio.com/download)

## Browser
Untuk membuka dan menampilkan dokumen HTML, digunakanlah browser.
Ada banyak jenis browser, tetapi berikut yang direkomendasi.
* [Google Chrome](https://www.google.com/chrome)
* [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/new/)

## Kenapa HTML?
HTML adalah dasar dari web.
Itulah yang kita lihat setiap kali kita mengunjungi situs favorit kita.
Seorang developer web tentu saja harus mengetahui apa itu HTML,
dan bagaimana cara membaca dan menggunakannya.

## Latihan: Hello World
Buat file bernama `index.html`, lalu isi dengan kode berikut.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hello, world!</title>
  </head>
  <body>
    Isi laman.
  </body>
</html>
```

Kemudian buka file tersebut menggunakan browser.

# Tags
HTML menggunakan "tags" untuk memberi tahu struktur laman kepada browser.
Biasanya, tag memiliki tag pembuka dan tag penutup.
Ada banyak jenis tags, tiap jenisnya memiliki fungsi berbeda.

Anggap tags sebagai seleksi teks pada teks editor.
Pada teks editor seperti MS Word, jika ingin mempertebal (bold) teks,
maka kita menyeleksi teks kemudan memberi efek bold.
Di HTML, tag digunakan untuk itu.

```
              ┌── "seleksi" teks ─┐
              ↓                   ↓
Lorem ipsum <tag>dolor sit amet</tag>, consectetur adipisicing elit.
```

Tag pembuka memiliki nama, seperti p, dikelilingi oleh kurung siku (`<>`).
Sebagai contoh, tag `<p>` memberi tahu browser permulaan paragraf.
Tag penutup memiliki hampir sama, dengan garis miring sebelum nama tag (`/`).
Tag `</p>` memberi tahu browser bahwa paragraf sudah selesai.


```html
<p>Ini adalah paragraf.</p>
```
Tag seperti `<br>`, `<img>` and `<hr>`, dapat digunakan tanpa tag penutup.

* `<br>` membuat garis baru pada teks.
* `<img>` membuat gambar dalam laman. Akan dijelaskan di lain bagian.
* `<hr>` merepresentasikan jeda antara tags. Pada veri HTML yang dahulu,
digunakan untuk merepresentasikan garis horizontal.
Browser mungkin akan menampilkan garis horizontal.  
Bisa digunakan seperti ini:
```html
<p>Paragraf pertama</p>
<hr>
<p>Paragraf kedua</p>
```

## HTML sederhana
Pada latihan sebelumnya, kita telah membuat dokumen HTML.
Tag `<html>` merangkup seluruh dokumen; memberi tahu dimana laman dimulai,
dan dimana laman selesai.

Ada juga tag `<head>` dan `<body>`.
Pada tag `<head>` kita menaruh informasi, seperti metadata dan `<title>`.
Isi `<title>` akan ditampilkan oleh tab browser.
Dalam tag `<body>` kita menaruh semua yang ingin ditampilkan oleh browser:
judul, teks, gambar, dll.

Mungkin kalian bertanya "Apa guna tag `<meta charset="utf-8">`?"
Ini adalah tag spesial, atau disebut juga meta-tag.
Meta-tag digunakan untuk memberi informasi tentang dokumen.
Anggap meta-tag sebagai cara untuk memberi informasi tentang informasi.
Kali ini artinya laman kita menggunakan charset `utf-8`.

## Latihan: Paragraf
Pada dokumen HTML dari latihan yang sebelummnya,
buatlah dua paragraf `<p>` berisi apapun didalam tag `<body>`.
Kemudian pisahkanlah kedua paragraf tersebut menggunakan tag `<hr>`.

# Atribut
Terkadang tag memerlukan atribut untuk mengatur hal seperti warna, ukuran, dll.
Tag dapat didefinisikan seperti:

```html
<tag atribut1="nilai1" atribut2="nilai2">isi tag</tag>
<!-- atau, untuk tag tanpa penutup -->
<tag atribut1="nilai1" atribut2="nilai2">
```

Menurut spesifikasi HTML, semua nilai harus dibuat diantara kutip ganda
(`atribut="nilai"`) atau kutip satu (`atribut='nilai'`).
Tidak menggunakan kutip tidak akan menjadi masalah,
karena browser akan memperbaikinya secara otomatis.
Tetapi, kita akan kesulitan jika nilai yang didefinisikan memerlukan spasi.

```html
<!-- kutip ganda -->
<tag atribut="nilai dengan spasi"></tag>
<!-- with single quotes -->
<tag atribut='nilai dengan spasi'></tag>
<!-- without quotes -->
<tag atribut=nilai dengan spasi></tag>
```

Pada contoh diatas, hanya "nilai" yang akan dianggap sebagai nilai dari atribut.
"dengan" dan "spasi" tidak akan diacuhkan oleh browser.
Karena itu, agar konsisten, lebih baik selalu menggunakan kutip.

Jika kita tidak mendefiniskan nilai atribut,
browser akan menggunakan nilai standar untuk atribut tersebut.
Urutan pendefinisian atribut tidak penting.

## Latihan: Atribut
Sebelumnya kita sudah mengetahui tag `<img>`.
Tag ini mencantumkan gambar pada laman web.
Beberapa atribut yang dibutuhkan untuk didefiniskan oleh tag ini adalah:

* `src` alamat gambar
* `alt` deskripsi gambar, akan ditampilkan jika `src` salah
* `height` tinggi dari elemen gambar
* `width` lebar dari elemen gambar

Buatlah dokumen HTML berisi gambar menggunakan tag `<img>`.

# Huruf
## Teks tebal dan miring
Jika ingin membuat teks dengan huruf tebal, maka kita membutuhkan tag
`<strong>` atau `<b>`, seperti:

```html
<p>Beberapa teks dengan kata <strong>penting</strong>.</p>
```

Hasil:
> Beberapa teks dengan kata **penting**.

Garis miring menggunakan tag `<em>` atau `<i>`:

```html
<p>Teks dengan <em>garis miring</em>.</p>
```

Hasil:

> Teks dengan _garis miring_.


Meskipun menghasilkan hasil yang sama, tag-tag tersebut tidaklah sama.
`<b>` hanya menghasilkan huruf tebal,
sedangkan `<strong>` merpresentasikan teks yang memerlukan atensi.
`<i>` pun hanya menghasilkan teks dnengan garis miring,
sedangkan `<em>` adalah simbol untuk teks emosional.

## Teks atas dan bawah
Menggeser teks ke atas ataupun bawah sering digunakan dalam matematika, fisika,
kimia. Tentu saja ada tag khusus untuk itu:

* `<sup>` teks atas
* `<sub>` teks bawah

Teks yang berada di dalam tag ini akan menjadi kecil. Misalnya:

```html
<p>Formula oksigen adalah H<sub>2</sub>O</p>

<p>Rumus Pythagoras: a<sup>2</sup> + b<sup>2</sup> = c<sup>2</sup></p>
```

Hasil:
><p>Formula oksigen adalah H<sub>2</sub>O</p>
><p>Rumus Pythagoras: a<sup>2</sup> + b<sup>2</sup> = c<sup>2</sup></p>

## Mengelompokkan teks
Perlu mengelompokkan teks menggunakan tag yang tidak merepresentasikan apapun.
Itulah gunanya tag `<span>`. Untuk sekarang, mungkin terasa tidak berguna,
tapi tag `<span>` adalah tag yang paling sering digunakan.

## Hal-hal yang perlu diingat
Berapapun banyaknya, spasi akan dibuat menjadi satu oleh browser.
Tiga contoh berikut akan memiliki hasil yang sama pada browser.

```html
<p>satu dua tiga<p>
<p>satu  dua  tiga<p>
<p>satu    dua    tiga<p>
```

HTML tidak akan memecah kalimat berdasarkan garis sambung,
seperti teks editor kebanyakan.

Simbol dapat ditulis menggunakan nama: `&nama;`
Nama adalah nama simbol tersebut. Beberapa contoh:

* `&&#x200b;quot;` kutip ganda (`"`)
* `&&#x200b;apos;` kutip satu (`'`)
* `&&#x200b;amp;` tanda dan (`&`)
* `&&#x200b;lt;` tanda kurang-dari (`<`)
* `&&#x200b;gt;` tanda lebih-dari (`>`)

Referensi:
https://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references

## Latihan: Huruf
Buatlah 5 paragraf, masing-masing berisi tag `<strong>`, `<em>`, `<sup>`,
`<sub>`, dan `<span>`.

# Judul
Dalam pembuatan artikel, seringkali kita membutuhkan pemisahan bagian.
HTML menyediakan enam tingkatan judul.
Setiap tingkat memperkecil kepentingan, dan memperkecil ukuran font.
Judul ini dapat kita gunakan sebagai pembagi bab.

Pada koran biasanya judul adalah hal yang dicetak paling besar,
sehingga menarik minat pembaca kemudian tertuliskan:
"Ini adalah hal penting".

Berikut penggunaan setiap tingkatan judul:

```html
<h1>Judul tingkat 1</h1>
<h2>Judul tingkat 2</h2>
<h3>Judul tingkat 3</h3>
<h4>Judul tingkat 4</h4>
<h5>Judul tingkat 5</h5>
<h6>Judul tingkat 6</h6>
```

Meskipun judul memperbesar ukuran font,
jangan gunakan judul untuk menulis paragraf dengan huruf besar.

Hindari melewatkan tingkatan judul. Selalu mulai dari `<h1>`,
kemudian lanjutkan dengan menggunakan `<h2>`, dan seterusnya.

Biasanya, judul hingga level ke-3 sudah cukup.
Jarang sekali level yang lebih dalam dibutuhkan.

## Latihan: Judul

Buatlah dokumen HTML dengan struktur berikut.
Tanda `#` menunjukkan tingkatan judul.

```markdown
# Judul
## Ringkasan
Teks...

## Contoh
### Contoh 1
Teks...

### Contoh 2
Teks...

## Lihat juga
Teks...
```

# Daftar
Terkadang, lebih mudah merepresentasikan data dengan daftar.
HTML sudah menyediakan tag untuk itu.
Ada 3 jenis daftar di HTML:

- daftar berurutan
- daftar tak berurutan
- daftar definisi

## Daftar Berurutan
Tag `<ol>` digunakan untuk daftar berurutan, biasanya dibuat dengan nomor.
`<ol>` adalah kependekan dari "ordered list".

Tiap butir pada daftar dapat direpresentasikan dengan tag `<li>`.
Contoh penggunaan:

```html
<ol>
  <li>butir pertama</li>
  <li>butir kedua</li>
  <li>butir ketiga</li>
</ol>
```

Hasil:
><ol>
>  <li>butir pertama</li>
>  <li>butir kedua</li>
>  <li>butir ketiga</li>
></ol>

Penomoran daftar dapat diganti menggunakan huruf, angka Romawi, bahkan titik.
Beberapa atribut tag `<ol>` yang sering digunakan:

- `type` jenis penomoran:
  - `1` angka (default)
  - `a` huruf kecil
  - `A` huruf besar
  - `i` angka Romawi dengan huruf kecil
  - `I` angka Romawi dengan huruf besar
- `start` menandakan awal hitungan daftar,
meskipun menggunakan penomoran romawi ataupun huruf,
atribut ini tetap didefinisikan dengan angka
- `reversed` menandakan apakah daftar dimulai dari yang terbesar atau tidak,
atribut ini menerima `true` atau `false` sebagai nilai.

## Daftar Tidak Berurutan
Daftar tak berurutan hampir sama dengan daftar berurutan,
tiap butirnya tetap didefinisikan menggunakan tag `<li>`.
Bedanya, tag yang digunakan adalah `<ul>`, bukan `<ol>`.
`<ul>` adalah kependekan dari "unordered list".
Biasanya, butir daftar tidak berurutan menggunakan titik saja.
Gunakanlah tag `<ul>` saat urutan butir pada daftar tidak terlalu penting.

```html
<ul>
  <li>butir pertama</li>
  <li>butir kedua</li>
  <li>butir ketiga</li>
</ul>
```

Hasil:
><ul>
>  <li>butir pertama</li>
>  <li>butir kedua</li>
>  <li>butir ketiga</li>
></ul>

Tag `<ul>` menerima atribut `type` dengan nilai:
- `circle` bulat kosong
- `disc` bulat penuh
- `square` kotak penuh

## Daftar Definisi
Mungkin butuh membuat daftar tanpa penanda butir, untuk keperluan definisi.
Untuk itulah tag `<dl>` digunakan, beserta tag `<dt>` dan `<dl>`.
Istilah berada di dalam tag `<dt>`, sedangkan definisinya pada tag `<dd>`.

```html
<dl>
  <dt>HTML</dt>
  <dd>
    Bahasa untuk membuat laman web.
  </dd>
</dl>
```

><dl>
>  <dt>HTML</dt>
>  <dd>
>    Bahasa untuk membuat laman web.
>  </dd>
></dl>

## Latihan: Daftar
Buat dokumen HTML berisi tiga jenis daftar:
1. daftar berurutan dengan 3 butir
2. daftar tidak berurutan dengan 3 butir
3. daftar definisi dengan 2 butir

# Tabel
Pada bab ini kita akan mempelajari tabel pada HTML.
Perhatikan tabel berikut:
```
╔═════╦═════╗
║ 1.1 ║ 1.2 ║
╠═════╬═════╣
║ 2.1 ║ 2.2 ║
╚═════╩═════╝
```

Tabel ini memiliki dimensi 2×2, atau dua baris dan dua kolom, seperti ini:
```
╔═════════════════════╗
║ baris 1             ║
║┏━━━━━━━━━┳━━━━━━━━━┓║
║┃ kolom 1 ┃ kolom 2 ┃║
║┗━━━━━━━━━┻━━━━━━━━━┛║
╠═════════════════════╣
║ baris 2             ║
║┏━━━━━━━━━┳━━━━━━━━━┓║
║┃ kolom 1 ┃ kolom 2 ┃║
║┗━━━━━━━━━┻━━━━━━━━━┛║
╚═════════════════════╝
```

Seperti inilah tabel pada HTML. Tabel dibuat menggunakan tag `<table>`,
baris dengan `<tr>`, dan kolom dengan `<td>`.
Jika kita menggunakan HTML untuk merepresentasikannya, akan jadi seperti:
```
<table>
╔═════════════════════╗
║ <tr>                ║
║┏━━━━━━━━━┳━━━━━━━━━┓║
║┃ <td>    ┃ <td>    ┃║
║┃   1.1   ┃   1.2   ┃║
║┃ </td>   ┃ </td>   ┃║
║┗━━━━━━━━━┻━━━━━━━━━┛║
║ </tr>               ║
╠═════════════════════╣
║ <tr>                ║
║┏━━━━━━━━━┳━━━━━━━━━┓║
║┃ <td>    ┃ <td>    ┃║
║┃   2.1   ┃   2.2   ┃║
║┃ </td>   ┃ </td>   ┃║
║┗━━━━━━━━━┻━━━━━━━━━┛║
║ </tr>               ║
╚═════════════════════╝
</table>
```

Dan berikut kode HTML untuk tabel diataas:
```html
<table>
  <tr>
    <td>1.1</td>
    <td>1.2</td>
  </tr>
  <tr>
    <td>2.1</td>
    <td>2.2</td>
  </tr>
</table>
```

Untuk baris judul, kita dapat menggunakan tag `<th>`, seperti:
```html
<p>Tabel dengan judul</p>
<table>
  <tr>
    <th>Nama</th>
    <th>Kota</th>
  </tr>
  <tr>
    <td>Janu</td>
    <td>Jakarta</td>
  </tr>
  <tr>
    <td>Ben</td>
    <td>Bandung</td>
  </tr>
</table>
```
Isi dari tag `<th>` akan dibuat lebih tebal.

Setiap tag tabel memiliki atribut yang sangat banyak, bacalah referensi dari:

* `<table>`
(https://developer.mozilla.org/en/docs/Web/HTML/Element/table#Attributes)
* `<tr>`
(https://developer.mozilla.org/en/docs/Web/HTML/Element/tr#Attributes)
* `<td>`
(https://developer.mozilla.org/en/docs/Web/HTML/Element/td#Attributes)
* `<th>`
(https://developer.mozilla.org/en/docs/Web/HTML/Element/th#Attributes)

## Latihan: Tabel

Buatlah tabel berikut:
```
╔══════════╦══════════╦══════════╗
║ WIB      ║ WITA     ║ WIT      ║
╠══════════╬══════════╬══════════╣
║ Bandung  ║ Denpasar ║ Ambon    ║
╠══════════╬══════════╬══════════╣
║ Jakarta  ║ Makassar ║ Jayapura ║
╠══════════╬══════════╬══════════╣
║ Medan    ║ Tarakan  ║ Merauke  ║
╚══════════╩══════════╩══════════╝
```

Baris pertama adalah baris judul, jadi gunakanlah tag `<th>`.

# Blok-blok HTML
Setelah pembuatan teks dasar pada HTML, kita perlu membuat struktur laman.
Masalahnya adalah, tidak semuanya bisa dibuat menggunakan blok HTML sederhana.

## Tag <div>
Mari mengenal tag `<div>` terlebih dahulu.
Tag ini digunakan untuk merepresentasikan wadah serbaguna.
Bisa digunakan untuk mengelompokkan tag-tag lain,
atau bagian dokumen yang menggunakan bahasa lain.

Contoh penggunaan:

```html
<div>
  <p>Any kind of content here. Such as

&lt;p&gt;, &lt;table&gt;. You name it!</p>
</div>
```

Preview:

**|>** Any kind of content here. Such as <p>, <table>. You name it!
